angular.module('regFalhas.controllers', [])

.controller('FalhasCtrl', function($scope, Falhas) {
  $scope.falhas = Falhas.all();
  console.log($scope.falhas);

  $scope.remove = function(falha) {
     Falhas.remove(falha);
  };
})

.controller('LoginCtrl', function($scope, $location, $firebaseAuth) {

  $scope.login = function(email, pass) {
    $scope.err = null;
    $firebaseAuth().$signInWithEmailAndPassword(email, pass)
      .then(function(user) {
        console.log('success! user.uid = ' + user.uid);

        $location.path('/tab/falhas');
      }, function(err) {
        console.error('error in login ' + err);
        $scope.err = err;
      });
  };  

  $scope.logoff = function() {
    $firebaseAuth().signout();
  }
})

.controller('FalhaDetailCtrl', function($scope, $stateParams, Falhas) {
  console.log($stateParams);
  console.log($stateParams.falhaId);
  $scope.falha = Falhas.get($stateParams.falhaId);
})

.controller('NovaFalhaCtrl', function($scope, Equipamentos, $state, $cordovaCamera, $cordovaFile, $location, $cordovaGeolocation) {
  
  $scope.equipamentos = Equipamentos.all();

  $scope.imagem = '';
  $scope.latitude = '';
  $scope.longitude = '';
  $scope.endereco = '';

  $scope.novaFalha = function(descricao, tipoEquipamento) {

    console.log("["  + descricao + "] [" + tipoEquipamento + "] [" + $scope.imagem + "] [" +
                      $scope.latitude + "] [" + $scope.longitude + "] [" + $scope.endereco + "]");

    var ref = firebase.database().ref().child("falhas");
    var novaFalhaRef = ref.push();

    novaFalhaRef.set({ 'descricao': descricao, 'tpequip': tipoEquipamento, 'imagem' : $scope.imagem,
                       'latitude' : $scope.latitude, 'longitude' : $scope.longitude, 'endereco' : $scope.endereco });

    this.descricao = null;
    this.tipo_equipamento = null;
    $scope.imagem = null;
    $scope.latitude = null;
    $scope.longitude = null;
    $scope.endereco = null;

    //$state.go('tab.falhas');
    $location.path('/tab/falhas');
  }
 
  $scope.addImage = function() {
    var options = {
      destinationType : Camera.DestinationType.DATA_URL,
      sourceType : Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
      allowEdit : false,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
    };
    
    $cordovaCamera.getPicture(options).then(function(imageData) {
     
      onImageSuccess(imageData);
 
      function onImageSuccess(imageData) {
        $scope.imagem = imageData;
      }
 
    }, function(err) {
      console.error(err);
    });
  }

  // isso é necessário para que o código execute sempre que a view for exibida
  $scope.$on('$ionicView.enter', function() {
    console.log('view again');

    $cordovaGeolocation
      .getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
      .then(function (position) {
        console.log(position);
        $scope.latitude = position.coords.latitude;
        $scope.longitude = position.coords.longitude;

        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng($scope.latitude, $scope.longitude);
        var request = { latLng: latlng };
        geocoder.geocode(request, function(data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[0] != null) {
              console.log(data);
              $scope.endereco = data[0].formatted_address;
            } else {
              $scope.endereco = '???';
            }
          }
        });
        console.log(position);
      }, function(err) {
        // error
        console.error('nao foi possivel obter as coordenadas');
      });
  });


  /**
  $window.navigator.geolocation.getCurrentPosition(function(position) {
    
    $scope.$apply(function() {

      $scope.latitude = position.coords.latitude;
      $scope.longitude = position.coords.longitude;

      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng($scope.latitude, $scope.longitude);
      
      var request = { latLng: latlng };

      geocoder.geocode(request, function(data, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (data[0] != null) {
            console.log(data);
            $scope.endereco = data[0].formatted_address;
          } else {
            $scope.endereco = '???';
            $scope.city = '???';
          }
        }
      });

      console.log(position);
    })
  })
*/
  
})

.controller('CameraCtrl', function($scope, $cordovaCamera, $cordovaFile) {
  $scope.images = [];
 
  $scope.addImage = function() {
    var options = {
      destinationType : Camera.DestinationType.NATIVE_URI,
      sourceType : Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
      allowEdit : false,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
    };
    
    $cordovaCamera.getPicture(options).then(function(imageData) {
     
      onImageSuccess(imageData);
 
      function onImageSuccess(fileURI) {
        createFileEntry(fileURI);
      }
 
      function createFileEntry(fileURI) {
        window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
      }
      
      function copyFile(fileEntry) {
        var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
        var newName = makeid() + name;
 
        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(fileSystem2) {
          fileEntry.copyTo(fileSystem2, newName, onCopySuccess, fail);
        }, fail);
    }
    
    function onCopySuccess(entry) {
      $scope.$apply(function () {
        $scope.images.push(entry.nativeURL);
      });
    }
 
    function fail(error) {
      console.log("fail: " + error.code);
    }
   
    function makeid() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
      for (var i=0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }
    }, function(err) {
      console.log(err);
    });
  }

  $scope.urlForImage = function(imageName) {
    var name = imageName.substr(imageName.lastIndexOf('/') + 1);
    var trueOrigin = cordova.file.dataDirectory + name;
    return trueOrigin;
  }
});