// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'regFalhas' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'regFalhas.services' is found in services.js
// 'regFalhas.controllers' is found in controllers.js
angular.module('regFalhas', ['ionic', 'regFalhas.controllers', 'regFalhas.services', 'firebase', 'ngCordova']) 

// .run(["$rootScope", "$state", function($rootScope, $state) {
  
//   $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
//     console.log('$stateChangeError --> ' + error);
//     // We can catch the error thrown when the $requireSignIn promise is rejected
//     // and redirect the user back to the home page
//     if (error === "AUTH_REQUIRED") {
//       //$state.go("tab.login");
//     }
//   });
// }])

.run(function($rootScope, $state, $window) {
  
  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    console.log('$stateChangeError --> ' + error);
    // We can catch the error thrown when the $requireSignIn promise is rejected
    // and redirect the user back to the home page
    if (error === "AUTH_REQUIRED") {
      $state.go("tab.login");
      $window.location.reload();
    }
  });

})

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.login', {
    url: '/login',
    views: {
      'tab-login': {
        templateUrl: 'templates/tab-login.html',
        controller: 'LoginCtrl'
      }
    }
  })

  .state('tab.falhas', {
      url: '/falhas',
      views: {
        'tab-falhas': {
          templateUrl: 'templates/tab-falhas.html',
          controller: 'FalhasCtrl'
        }
      },
      resolve: {
        // controller will not be loaded until $requireSignIn resolves
        "currentAuth": function($firebaseAuth) {
          // $requireSignIn returns a promise so the resolve waits for it to complete
          // If the promise is rejected, it will throw a $stateChangeError (see above)
          return $firebaseAuth().$requireSignIn();
        }
      }
    })
  .state('tab.falha-detail', {
      url: '/falhas/:falhaId',
      views: {
        'tab-falhas': {
          templateUrl: 'templates/falha-detail.html',
          controller: 'FalhaDetailCtrl'
        }
      },
      resolve: {
        // controller will not be loaded until $requireSignIn resolves
        "currentAuth": function($firebaseAuth) {
          // $requireSignIn returns a promise so the resolve waits for it to complete
          // If the promise is rejected, it will throw a $stateChangeError (see above)
          return $firebaseAuth().$requireSignIn();
        }
      }
    })


  .state('tab.nova-falha', {
    url: '/nova-falha',
    views: {
      'tab-nova-falha': {
        templateUrl: 'templates/tab-nova-falha.html',
        controller: 'NovaFalhaCtrl'
      }
    },
    resolve: {
        // controller will not be loaded until $requireSignIn resolves
        "currentAuth": function($firebaseAuth) {
          // $requireSignIn returns a promise so the resolve waits for it to complete
          // If the promise is rejected, it will throw a $stateChangeError (see above)
          return $firebaseAuth().$requireSignIn();
        }
      }
  })

  .state('tab.camera', {
    url: '/camera',
    views: {
      'tab-camera': {
        templateUrl: 'templates/tab-camera.html',
        controller: 'CameraCtrl'
      }
    }});


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/login');

});
