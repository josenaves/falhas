var mServices = angular.module('regFalhas.services', [])

.factory('Falhas', function($firebaseArray) {
  // Might use a resource here that returns a JSON array

  // pega referencia para o nó 'falhas'
  var ref = firebase.database().ref().child("falhas");
  falhas = $firebaseArray(ref);

  return {
    all: function() {
      return falhas;
    },
    remove: function(falha) {
      falhas.$remove(falha);
      falhas.splice(falhas.indexOf(falha), 1);
    },
    get: function(falhaId) {
      for (var i = 0; i < falhas.length; i++) {
        if (falhas[i].$id === falhaId) {
          return falhas[i];
        }
      }
      return null;
    }
  };
})
.factory('Auth', function($firebaseAuth) {
    return $firebaseAuth();
})
.factory('Equipamentos', function($firebaseArray) {
      var ref = firebase.database().ref().child("tipo_equipamento");
      var equipamentos = $firebaseArray(ref); 

      return {
        all: function() {
          console.log(equipamentos);
          return equipamentos;
        }
      }
});
